FROM mysql:8

ENV MYSQL_ROOT_PASSWORD=pass
ENV MYSQL_DATABASE=metexplore_test

COPY --chmod=777 docker-healthcheck .
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD ./docker-healthcheck

ADD ./1-metexplore_test-defs.sql /docker-entrypoint-initdb.d/
ADD ./2-metexplore_test-data.sql /docker-entrypoint-initdb.d/

