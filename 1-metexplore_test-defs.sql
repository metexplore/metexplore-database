-- MariaDB dump 10.19  Distrib 10.6.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: metexplore_test
-- ------------------------------------------------------
-- Server version	10.6.18-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `metexplore_test`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `metexplore_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `metexplore_test`;

--
-- Table structure for table `Analysis`
--

DROP TABLE IF EXISTS `Analysis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Analysis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp(),
  `date_edition` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1135 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AnalysisData`
--

DROP TABLE IF EXISTS `AnalysisData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnalysisData` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `type` enum('image','datafile','dataset','plot','viz','other') NOT NULL DEFAULT 'other',
  `json` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=371 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AnalysisDataStep`
--

DROP TABLE IF EXISTS `AnalysisDataStep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnalysisDataStep` (
  `id_data` int(11) NOT NULL,
  `id_step` int(11) NOT NULL,
  KEY `fk_foreign_key_data` (`id_data`),
  KEY `fk_foreign_key_step` (`id_step`),
  CONSTRAINT `fk_foreign_key_data` FOREIGN KEY (`id_data`) REFERENCES `AnalysisData` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_foreign_key_step` FOREIGN KEY (`id_step`) REFERENCES `AnalysisStep` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AnalysisJobStep`
--

DROP TABLE IF EXISTS `AnalysisJobStep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnalysisJobStep` (
  `id_app` int(11) NOT NULL,
  `id_step` int(11) NOT NULL,
  `parameters` longtext DEFAULT NULL,
  KEY `fk_foreign_key_app` (`id_app`),
  KEY `fk_foreign_key_step_job` (`id_step`),
  CONSTRAINT `fk_foreign_key_app` FOREIGN KEY (`id_app`) REFERENCES `MetExploreApp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_foreign_key_step_job` FOREIGN KEY (`id_step`) REFERENCES `AnalysisStep` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AnalysisStep`
--

DROP TABLE IF EXISTS `AnalysisStep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnalysisStep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `id_analysis` int(11) NOT NULL,
  `type` enum('data','job') NOT NULL DEFAULT 'data',
  `root` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Indicates if this step is the first step of the analysis',
  PRIMARY KEY (`id`),
  KEY `fk_foreign_key_analysis` (`id_analysis`),
  CONSTRAINT `fk_foreign_key_analysis` FOREIGN KEY (`id_analysis`) REFERENCES `Analysis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=556 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AnalysisStepLink`
--

DROP TABLE IF EXISTS `AnalysisStepLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnalysisStepLink` (
  `id_source` int(11) NOT NULL,
  `id_target` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  KEY `fk_foreign_key_step_source` (`id_source`),
  KEY `fk_foreign_key_step_target` (`id_target`),
  CONSTRAINT `fk_foreign_key_step_source` FOREIGN KEY (`id_source`) REFERENCES `AnalysisStep` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_foreign_key_step_target` FOREIGN KEY (`id_target`) REFERENCES `AnalysisStep` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AnalysisUserRights`
--

DROP TABLE IF EXISTS `AnalysisUserRights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnalysisUserRights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_analysis` int(11) NOT NULL,
  `mode` enum('read','write','admin') NOT NULL DEFAULT 'read',
  PRIMARY KEY (`id`),
  KEY `fk_foreign_key_analysis_rights` (`id_analysis`),
  KEY `fk_foreign_key_user_analysis_rights` (`id_user`),
  CONSTRAINT `fk_foreign_key_analysis_rights` FOREIGN KEY (`id_analysis`) REFERENCES `Analysis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_foreign_key_user_analysis_rights` FOREIGN KEY (`id_user`) REFERENCES `UserMetExplore3` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1072 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ApiAppToken`
--

DROP TABLE IF EXISTS `ApiAppToken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ApiAppToken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `institution` varchar(100) NOT NULL DEFAULT 'Not defined',
  `lab` varchar(100) NOT NULL DEFAULT 'Not defined',
  `description` text NOT NULL,
  `token` varchar(200) NOT NULL,
  `protected` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=20067 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AssociatedFiles`
--

DROP TABLE IF EXISTS `AssociatedFiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssociatedFiles` (
  `id` int(11) NOT NULL,
  `path` varchar(200) NOT NULL,
  `legend` varchar(200) DEFAULT NULL,
  `id_exp` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Biblio`
--

DROP TABLE IF EXISTS `Biblio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Biblio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pubmed_id` int(11) DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `authors` text DEFAULT NULL,
  `journal` varchar(300) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `short_ref` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Catalyses`
--

DROP TABLE IF EXISTS `Catalyses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Catalyses` (
  `id_reaction_in_network` int(11) NOT NULL,
  `id_enzyme` int(11) NOT NULL,
  KEY `fk_Catalyses_ReactionInBioSource1` (`id_reaction_in_network`),
  KEY `fk_Catalyses_EnzymeInBioSource1` (`id_enzyme`),
  CONSTRAINT `fk_Catalyses_Enzyme` FOREIGN KEY (`id_enzyme`) REFERENCES `Enzyme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Catalyses_ReactionInBioSource1` FOREIGN KEY (`id_reaction_in_network`) REFERENCES `ReactionInNetwork` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Collection`
--

DROP TABLE IF EXISTS `Collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT 'NA',
  `description` text DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24826 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Comment`
--

DROP TABLE IF EXISTS `Comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(45) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Compartment`
--

DROP TABLE IF EXISTS `Compartment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Compartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_identifier` varchar(1000) NOT NULL DEFAULT 'NA',
  `name` varchar(100) NOT NULL,
  `sbo` varchar(20) DEFAULT NULL,
  `id_network` int(11) NOT NULL,
  `spatial_dimensions` float NOT NULL DEFAULT 3,
  `units` varchar(45) NOT NULL DEFAULT 'dimensionless',
  `constant` tinyint(1) NOT NULL DEFAULT 1,
  `size` float NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_foreign_key_compartment_biosource` (`id_network`),
  CONSTRAINT `fk_foreign_key_compartment_biosource` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6436 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CompartmentInCompartment`
--

DROP TABLE IF EXISTS `CompartmentInCompartment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CompartmentInCompartment` (
  `id_compartment_in` int(11) NOT NULL,
  `id_compartment_outside` int(11) NOT NULL,
  KEY `fk_CompartmentInCompartment_CompartmentInBioSource1` (`id_compartment_in`),
  KEY `fk_CompartmentInCompartment_CompartmentInBioSource2` (`id_compartment_outside`),
  CONSTRAINT `fk_CompartmentInCompartment_CompartmentIn` FOREIGN KEY (`id_compartment_in`) REFERENCES `Compartment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CompartmentInCompartment_CompartmentOutside` FOREIGN KEY (`id_compartment_outside`) REFERENCES `Compartment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Document`
--

DROP TABLE IF EXISTS `Document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_comment` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `date_creation` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_Document_Comment1_idx` (`id_comment`),
  CONSTRAINT `fk_Document_Comment1` FOREIGN KEY (`id_comment`) REFERENCES `Comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Enzyme`
--

DROP TABLE IF EXISTS `Enzyme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Enzyme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `db_identifier` varchar(1000) NOT NULL,
  `sbo` varchar(20) DEFAULT NULL,
  `id_network` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foreign_key_biosource_enzyme` (`id_network`),
  KEY `name` (`name`(768)) USING BTREE,
  KEY `dbIdentifier` (`db_identifier`(768)) USING BTREE,
  CONSTRAINT `fk_foreign_key_biosource_enzyme` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=341674 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EnzymeHasComment`
--

DROP TABLE IF EXISTS `EnzymeHasComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EnzymeHasComment` (
  `id_enzyme` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  KEY `fk_EnzymeHasComment_Enzyme1_idx` (`id_enzyme`),
  KEY `fk_EnzymeHasComment_Comment1_idx` (`id_comment`),
  CONSTRAINT `fk_EnzymeHasComment_Comment1` FOREIGN KEY (`id_comment`) REFERENCES `Comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EnzymeHasComment_Enzyme1` FOREIGN KEY (`id_enzyme`) REFERENCES `Enzyme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Gene`
--

DROP TABLE IF EXISTS `Gene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Gene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `db_identifier` varchar(1000) DEFAULT NULL,
  `sbo` varchar(20) DEFAULT NULL,
  `id_network` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255)),
  KEY `dbIdentifier` (`db_identifier`(255)),
  KEY `fk_foreign_key_biosource_gene` (`id_network`),
  CONSTRAINT `fk_foreign_key_biosource_gene` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=504370 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GeneCodesForProtein`
--

DROP TABLE IF EXISTS `GeneCodesForProtein`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GeneCodesForProtein` (
  `id_gene` int(11) NOT NULL,
  `id_protein` int(11) NOT NULL,
  KEY `fk_GeneCodesForProtein_GeneInBioSource1` (`id_gene`),
  KEY `fk_GeneCodesForProtein_ProteinInBioSource1` (`id_protein`),
  CONSTRAINT `fk_GeneCodesForProtein_Gene` FOREIGN KEY (`id_gene`) REFERENCES `Gene` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_GeneCodesForProtein_Protein` FOREIGN KEY (`id_protein`) REFERENCES `Protein` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GeneHasComment`
--

DROP TABLE IF EXISTS `GeneHasComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GeneHasComment` (
  `idGene` int(11) NOT NULL,
  `idComment` int(11) NOT NULL,
  KEY `fk_GeneHasComment_Gene1_idx` (`idGene`),
  KEY `fk_GeneHasComment_Comment1_idx` (`idComment`),
  CONSTRAINT `fk_GeneHasComment_Comment1` FOREIGN KEY (`idComment`) REFERENCES `Comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_GeneHasComment_Gene1` FOREIGN KEY (`idGene`) REFERENCES `Gene` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GeneIdentifiers`
--

DROP TABLE IF EXISTS `GeneIdentifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GeneIdentifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_gene` int(11) NOT NULL,
  `ext_db_name` varchar(100) NOT NULL,
  `ext_id` mediumtext NOT NULL,
  `origin` varchar(100) NOT NULL DEFAULT 'SBML',
  `score` int(11) NOT NULL DEFAULT 1,
  `id_metadata` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_GeneIds` (`id_gene`) USING BTREE,
  KEY `idMetadata` (`id_metadata`),
  CONSTRAINT `geneIdsConstraint` FOREIGN KEY (`id_gene`) REFERENCES `Gene` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9406200 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `GraphEdgesView`
--

DROP TABLE IF EXISTS `GraphEdgesView`;
/*!50001 DROP VIEW IF EXISTS `GraphEdgesView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `GraphEdgesView` AS SELECT
 1 AS `source`,
  1 AS `interaction`,
  1 AS `target`,
  1 AS `id_network`,
  1 AS `reversible` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `GraphMetaboliteNodesView`
--

DROP TABLE IF EXISTS `GraphMetaboliteNodesView`;
/*!50001 DROP VIEW IF EXISTS `GraphMetaboliteNodesView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `GraphMetaboliteNodesView` AS SELECT
 1 AS `db_identifier`,
  1 AS `name`,
  1 AS `pathways`,
  1 AS `compartments`,
  1 AS `side_compound`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `GraphReactionNodesView`
--

DROP TABLE IF EXISTS `GraphReactionNodesView`;
/*!50001 DROP VIEW IF EXISTS `GraphReactionNodesView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `GraphReactionNodesView` AS SELECT
 1 AS `db_identifier`,
  1 AS `name`,
  1 AS `pathways`,
  1 AS `id_network`,
  1 AS `reversible` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `History`
--

DROP TABLE IF EXISTS `History`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `History` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_network` int(11) DEFAULT NULL,
  `id_collection` int(11) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `action` varchar(200) NOT NULL,
  `object` varchar(45) DEFAULT NULL,
  `db_identifier` varchar(1000) DEFAULT NULL,
  `field` varchar(20) DEFAULT NULL,
  `old_value` varchar(1000) DEFAULT NULL,
  `new_value` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foreign_key_history_idBioSource` (`id_network`),
  KEY `fk_foreign_key_history_idDatabaseRef` (`id_collection`),
  CONSTRAINT `fk_foreign_key_history_idBioSource` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_foreign_key_history_idDatabaseRef` FOREIGN KEY (`id_collection`) REFERENCES `Collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListOfUnits`
--

DROP TABLE IF EXISTS `ListOfUnits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListOfUnits` (
  `id_unit_definition` int(11) NOT NULL,
  `id_unit` int(11) NOT NULL,
  KEY `idUnit` (`id_unit`),
  KEY `idUnitDefinition` (`id_unit_definition`),
  CONSTRAINT `ListOfUnits_ibfk_1` FOREIGN KEY (`id_unit_definition`) REFERENCES `UnitDefinition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ListOfUnits_ibfk_2` FOREIGN KEY (`id_unit`) REFERENCES `Unit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetExploreApp`
--

DROP TABLE IF EXISTS `MetExploreApp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetExploreApp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `sub_title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `route` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Metabolite`
--

DROP TABLE IF EXISTS `Metabolite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Metabolite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `chemical_formula` mediumtext DEFAULT NULL,
  `smiles` mediumtext DEFAULT NULL,
  `caas` varchar(200) DEFAULT NULL,
  `weight` varchar(45) DEFAULT NULL,
  `mono_iso_mass` varchar(45) DEFAULT NULL,
  `exact_neutral_mass` varchar(45) DEFAULT NULL,
  `average_mass` varchar(45) DEFAULT NULL,
  `generic` tinyint(1) DEFAULT NULL,
  `id_collection` int(11) DEFAULT NULL,
  `db_identifier` varchar(1000) DEFAULT NULL,
  `charge` int(11) DEFAULT NULL,
  `inchi` mediumtext DEFAULT NULL,
  `sbo` varchar(20) DEFAULT 'SBO_0000299',
  PRIMARY KEY (`id`),
  KEY `fk_Metabolite_Database` (`id_collection`),
  KEY `weight` (`weight`),
  KEY `dbIdentifier` (`db_identifier`(255)),
  CONSTRAINT `fk_Metabolite_Database` FOREIGN KEY (`id_collection`) REFERENCES `Collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12315194 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `MetaboliteCompartmentsView`
--

DROP TABLE IF EXISTS `MetaboliteCompartmentsView`;
/*!50001 DROP VIEW IF EXISTS `MetaboliteCompartmentsView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `MetaboliteCompartmentsView` AS SELECT
 1 AS `db_identifier`,
  1 AS `name`,
  1 AS `compartment_name`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `MetaboliteIdentifiers`
--

DROP TABLE IF EXISTS `MetaboliteIdentifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaboliteIdentifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_metabolite` int(11) NOT NULL,
  `ext_db_name` varchar(100) NOT NULL,
  `ext_id` mediumtext NOT NULL,
  `origin` varchar(100) NOT NULL DEFAULT 'SBML',
  `score` int(11) NOT NULL DEFAULT 1,
  `svg_file` varchar(100) DEFAULT NULL,
  `id_metadata` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_MetaboliteIds` (`id_metabolite`),
  KEY `idMetadata` (`id_metadata`),
  CONSTRAINT `fk_MetaboliteIds` FOREIGN KEY (`id_metabolite`) REFERENCES `Metabolite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12732778 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaboliteInCompartment`
--

DROP TABLE IF EXISTS `MetaboliteInCompartment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaboliteInCompartment` (
  `id_metabolite_in_network` int(11) NOT NULL,
  `id_compartment` int(11) NOT NULL,
  KEY `idMetabolite` (`id_metabolite_in_network`),
  KEY `idCompartmentInBioSource` (`id_compartment`),
  CONSTRAINT `MetaboliteInCompartment_Compartment` FOREIGN KEY (`id_compartment`) REFERENCES `Compartment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MetaboliteInCompartment_Metabolite` FOREIGN KEY (`id_metabolite_in_network`) REFERENCES `MetaboliteInNetwork` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaboliteInNetwork`
--

DROP TABLE IF EXISTS `MetaboliteInNetwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaboliteInNetwork` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_network` int(11) NOT NULL,
  `id_metabolite` int(11) NOT NULL,
  `boundary_condition` tinyint(1) DEFAULT NULL,
  `side_compound` tinyint(1) NOT NULL DEFAULT 0,
  `constant` tinyint(1) NOT NULL DEFAULT 0,
  `has_only_substance_unit` tinyint(1) NOT NULL DEFAULT 0,
  `substance_unit` varchar(100) DEFAULT NULL,
  `initial_quantity` varchar(100) DEFAULT NULL,
  `in_conflict_with` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_MetaboliteInOrg_BioSource` (`id_network`),
  KEY `fk_MetaboliteInOrg_Metabolite` (`id_metabolite`),
  CONSTRAINT `fk_MetaboliteInOrg_BioSource` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MetaboliteInOrg_Metabolite` FOREIGN KEY (`id_metabolite`) REFERENCES `Metabolite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12268122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaboliteInNetworkMetadata`
--

DROP TABLE IF EXISTS `MetaboliteInNetworkMetadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaboliteInNetworkMetadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_metabolite_in_network` int(11) NOT NULL,
  `metadata` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idMetaboliteInBioSource` (`id_metabolite_in_network`),
  CONSTRAINT `MetaboliteInNetworkMetadata_ibfk_1` FOREIGN KEY (`id_metabolite_in_network`) REFERENCES `MetaboliteInNetwork` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2053407 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `MetabolitePathwaysView`
--

DROP TABLE IF EXISTS `MetabolitePathwaysView`;
/*!50001 DROP VIEW IF EXISTS `MetabolitePathwaysView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `MetabolitePathwaysView` AS SELECT
 1 AS `db_identifier`,
  1 AS `name`,
  1 AS `pathway_name`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `MetaboliteSideCompoundView`
--

DROP TABLE IF EXISTS `MetaboliteSideCompoundView`;
/*!50001 DROP VIEW IF EXISTS `MetaboliteSideCompoundView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `MetaboliteSideCompoundView` AS SELECT
 1 AS `db_identifier`,
  1 AS `side_compound`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Network`
--

DROP TABLE IF EXISTS `Network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT 'NA',
  `id_collection` int(11) DEFAULT NULL,
  `db_identifier` varchar(100) DEFAULT NULL,
  `ncbi_id` int(11) NOT NULL DEFAULT 12908,
  `organism_name` varchar(100) NOT NULL DEFAULT 'Not defined',
  `strain` varchar(500) DEFAULT 'NA',
  `tissue` varchar(500) DEFAULT NULL,
  `source` varchar(200) DEFAULT NULL,
  `cell_type` varchar(100) DEFAULT NULL,
  `version` varchar(45) NOT NULL,
  `description` text DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp(),
  `date_last_modif` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `top` tinyint(1) NOT NULL DEFAULT 0,
  `visits` int(11) NOT NULL DEFAULT 0 COMMENT 'Number of times where the biosource has been selected',
  `url` varchar(100) NOT NULL,
  `identifier_origin` varchar(45) NOT NULL DEFAULT 'unknown',
  PRIMARY KEY (`id`),
  KEY `fk_BioSource_Database` (`id_collection`),
  CONSTRAINT `fk_BioSource_Database` FOREIGN KEY (`id_collection`) REFERENCES `Collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34701 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NetworkHasReference`
--

DROP TABLE IF EXISTS `NetworkHasReference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NetworkHasReference` (
  `id_network` int(11) NOT NULL,
  `id_biblio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NetworkInProject`
--

DROP TABLE IF EXISTS `NetworkInProject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NetworkInProject` (
  `id_network` int(11) NOT NULL,
  `id_project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
ALTER DATABASE `metexplore_test` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`metexplore`@`localhost`*/ /*!50003 TRIGGER `add_network_in_project` AFTER INSERT ON `NetworkInProject` FOR EACH ROW BEGIN



INSERT INTO UserNetwork (idUser, idNetwork, Acces) 

SELECT UserInProject.id_user, NEW.id_network, UserInProject.id_access FROM UserInProject WHERE id_project = NEW.id_project

ON DUPLICATE KEY UPDATE Acces = UserInProject.id_access;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `metexplore_test` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin ;
ALTER DATABASE `metexplore_test` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`metexplore`@`localhost`*/ /*!50003 TRIGGER `update_network_in_project` AFTER UPDATE ON `NetworkInProject` FOR EACH ROW BEGIN



INSERT INTO UserNetwork (idUser, idBioSource, Acces) 

SELECT UserInProject.id_user, NEW.id_network, UserInProject.id_access FROM UserInProject WHERE idProject = NEW.id_project

ON DUPLICATE KEY UPDATE Acces = UserInProject.id_access;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `metexplore_test` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin ;

--
-- Table structure for table `NetworkMetadata`
--

DROP TABLE IF EXISTS `NetworkMetadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NetworkMetadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_network` int(11) NOT NULL,
  `metadata` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idBioSource` (`id_network`),
  CONSTRAINT `NetworkMetadata_ibfk_1` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ParametersInReaction`
--

DROP TABLE IF EXISTS `ParametersInReaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ParametersInReaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reaction_in_network` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idReactionInBioSource` (`id_reaction_in_network`),
  CONSTRAINT `fk_foreign_parameter_reaction` FOREIGN KEY (`id_reaction_in_network`) REFERENCES `ReactionInNetwork` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pathway`
--

DROP TABLE IF EXISTS `Pathway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pathway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `db_identifier` varchar(1000) DEFAULT NULL,
  `id_network` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idBioSource` (`id_network`) USING BTREE,
  KEY `name` (`name`(768)),
  KEY `dbIdentifier` (`db_identifier`(768)) USING BTREE,
  CONSTRAINT `fk_foreign_pathway_biosource` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53483 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PathwayHasComment`
--

DROP TABLE IF EXISTS `PathwayHasComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PathwayHasComment` (
  `id_pathway` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  PRIMARY KEY (`id_pathway`,`id_comment`),
  KEY `fk_PathwayHasComment_Pathway1_idx` (`id_pathway`),
  KEY `fk_PathwayHasComment_Comment1_idx` (`id_comment`),
  CONSTRAINT `fk_PathwayHasComment_Comment1` FOREIGN KEY (`id_comment`) REFERENCES `Comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PathwayHasComment_Pathway1` FOREIGN KEY (`id_pathway`) REFERENCES `Pathway` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `PathwayMetabolitesView`
--

DROP TABLE IF EXISTS `PathwayMetabolitesView`;
/*!50001 DROP VIEW IF EXISTS `PathwayMetabolitesView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `PathwayMetabolitesView` AS SELECT
 1 AS `pathway_db_identifier`,
  1 AS `pathway_name`,
  1 AS `id_network`,
  1 AS `metabolite_db_identifier` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `PathwayReactionsView`
--

DROP TABLE IF EXISTS `PathwayReactionsView`;
/*!50001 DROP VIEW IF EXISTS `PathwayReactionsView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `PathwayReactionsView` AS SELECT
 1 AS `pathway_db_identifier`,
  1 AS `pathway_name`,
  1 AS `id_network`,
  1 AS `reaction_db_identifier` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Project`
--

DROP TABLE IF EXISTS `Project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `date_creation` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProjectHasComment`
--

DROP TABLE IF EXISTS `ProjectHasComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProjectHasComment` (
  `id_project` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  `id_type` int(11) NOT NULL DEFAULT 31,
  PRIMARY KEY (`id_project`,`id_comment`),
  KEY `fk_ProjectHasComment_Project1_idx` (`id_project`),
  KEY `fk_ProjectHasComment_Comment1_idx` (`id_comment`),
  KEY `fk_ProjectHasComment_Status1_idx` (`id_type`),
  CONSTRAINT `fk_ProjectHasComment_Comment1` FOREIGN KEY (`id_comment`) REFERENCES `Comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ProjectHasComment_Project1` FOREIGN KEY (`id_project`) REFERENCES `Project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ProjectHasComment_Status1` FOREIGN KEY (`id_type`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Protein`
--

DROP TABLE IF EXISTS `Protein`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Protein` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `db_identifier` varchar(1000) DEFAULT NULL,
  `sbo` varchar(20) DEFAULT NULL,
  `id_network` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255)),
  KEY `dbIdentifier` (`db_identifier`(255)),
  KEY `fk_foreign_key_biosource_protein` (`id_network`),
  CONSTRAINT `fk_foreign_key_biosource_protein` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=492987 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProteinHasComment`
--

DROP TABLE IF EXISTS `ProteinHasComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProteinHasComment` (
  `id_protein` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  KEY `fk_ProteinHasComment_Protein1_idx` (`id_protein`),
  KEY `fk_ProteinHasComment_Comment1_idx` (`id_comment`),
  CONSTRAINT `fk_ProteinHasComment_Comment1` FOREIGN KEY (`id_comment`) REFERENCES `Comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ProteinHasComment_Protein1` FOREIGN KEY (`id_protein`) REFERENCES `Protein` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProteinIdentifiers`
--

DROP TABLE IF EXISTS `ProteinIdentifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProteinIdentifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protein` int(11) NOT NULL,
  `ext_db_name` varchar(100) NOT NULL,
  `ext_id` mediumtext NOT NULL,
  `origin` varchar(100) NOT NULL DEFAULT 'SBML',
  `score` int(11) NOT NULL DEFAULT 1,
  `id_metadata` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_ProteinIds` (`id_protein`) USING BTREE,
  KEY `idMetadata` (`id_metadata`),
  CONSTRAINT `idProteinConstraint` FOREIGN KEY (`id_protein`) REFERENCES `Protein` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProteinInEnzyme`
--

DROP TABLE IF EXISTS `ProteinInEnzyme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProteinInEnzyme` (
  `id_protein` int(11) NOT NULL,
  `id_enzyme` int(11) NOT NULL,
  KEY `fk_ProteinInEnzyme_EnzymeInBioSource1` (`id_enzyme`),
  KEY `fk_ProteinInEnzyme_ProteinInBioSource1` (`id_protein`),
  CONSTRAINT `fk_ProteinInEnzyme_Enzyme` FOREIGN KEY (`id_enzyme`) REFERENCES `Enzyme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ProteinInEnzyme_Protein` FOREIGN KEY (`id_protein`) REFERENCES `Protein` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Reactant`
--

DROP TABLE IF EXISTS `Reactant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reactant` (
  `id_reaction` int(11) NOT NULL,
  `id_metabolite` int(11) NOT NULL,
  `coeff` varchar(25) DEFAULT '1',
  `cofactor` tinyint(1) DEFAULT 0,
  `side` enum('LEFT','RIGHT') NOT NULL DEFAULT 'LEFT',
  KEY `fk_LeftParticipant_Reaction` (`id_reaction`),
  KEY `fk_LeftParticipant_Metabolite` (`id_metabolite`),
  KEY `side` (`side`),
  CONSTRAINT `fk_LeftParticipant_Metabolite` FOREIGN KEY (`id_metabolite`) REFERENCES `Metabolite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_LeftParticipant_Reaction` FOREIGN KEY (`id_reaction`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Reaction`
--

DROP TABLE IF EXISTS `Reaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `spontaneous` tinyint(1) DEFAULT NULL,
  `generic` tinyint(1) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `ec` varchar(1000) DEFAULT NULL,
  `id_collection` int(11) DEFAULT NULL,
  `db_identifier` varchar(1000) DEFAULT NULL,
  `go` varchar(64) DEFAULT NULL,
  `goName` varchar(256) DEFAULT NULL,
  `id_last_annotator` int(11) DEFAULT NULL,
  `date_modification` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sbo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255)),
  KEY `ec` (`ec`(255)),
  KEY `dbIdentifier` (`db_identifier`(255)),
  KEY `fk_foreign_key_reaction_dbRef` (`id_collection`),
  CONSTRAINT `fk_foreign_key_reaction_dbRef` FOREIGN KEY (`id_collection`) REFERENCES `Collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11548618 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReactionHasComment`
--

DROP TABLE IF EXISTS `ReactionHasComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReactionHasComment` (
  `id_reaction` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  PRIMARY KEY (`id_reaction`,`id_comment`),
  KEY `fk_ReactionHasComment_Reaction1` (`id_reaction`),
  KEY `fk_ReactionHasComment_Comment1` (`id_comment`),
  CONSTRAINT `fk_ReactionHasComment_Comment1` FOREIGN KEY (`id_comment`) REFERENCES `Comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ReactionHasComment_Reaction1` FOREIGN KEY (`id_reaction`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReactionHasReference`
--

DROP TABLE IF EXISTS `ReactionHasReference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReactionHasReference` (
  `id_reaction` int(11) NOT NULL,
  `id_biblio` int(11) NOT NULL,
  KEY `fk_ReactionHasReference_Reaction1` (`id_reaction`),
  KEY `fk_ReactionHasReference_Biblio1` (`id_biblio`),
  CONSTRAINT `fk_ReactionHasReference_Biblio1` FOREIGN KEY (`id_biblio`) REFERENCES `Biblio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ReactionHasReference_Reaction1` FOREIGN KEY (`id_reaction`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReactionHasStatus`
--

DROP TABLE IF EXISTS `ReactionHasStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReactionHasStatus` (
  `id_reaction` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  KEY `fk_reactionHasConfidenceScore_Reaction1` (`id_reaction`),
  KEY `fk_reactionHasConfidenceScore_Status1` (`id_status`),
  CONSTRAINT `fk_reactionHasConfidenceScore_Reaction1` FOREIGN KEY (`id_reaction`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_reactionHasConfidenceScore_Status1` FOREIGN KEY (`id_status`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReactionIdentifiers`
--

DROP TABLE IF EXISTS `ReactionIdentifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReactionIdentifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reaction` int(11) NOT NULL,
  `ext_db_name` varchar(100) NOT NULL,
  `ext_id` mediumtext NOT NULL,
  `origin` varchar(100) NOT NULL DEFAULT 'SBML',
  `score` int(11) NOT NULL DEFAULT 1,
  `id_metadata` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_ReactionIds` (`id_reaction`) USING BTREE,
  KEY `idMetadata` (`id_metadata`),
  CONSTRAINT `idReactionConstraint` FOREIGN KEY (`id_reaction`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1367261 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReactionInNetwork`
--

DROP TABLE IF EXISTS `ReactionInNetwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReactionInNetwork` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reaction` int(11) NOT NULL,
  `id_network` int(11) NOT NULL,
  `reversible` tinyint(1) DEFAULT NULL,
  `hole` tinyint(1) DEFAULT NULL,
  `upper_bound` varchar(45) DEFAULT NULL,
  `lower_bound` varchar(45) DEFAULT NULL,
  `unit_definition_bounds` int(11) DEFAULT NULL,
  `fast` tinyint(1) NOT NULL DEFAULT 0,
  `kinetic_formula` varchar(1000) DEFAULT NULL,
  `note` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ReactionInOrg_Reaction` (`id_reaction`),
  KEY `fk_ReactionInOrg_BioSource` (`id_network`),
  KEY `boundsUnitDef` (`unit_definition_bounds`),
  CONSTRAINT `ReactionInNetwork_ibfk_1` FOREIGN KEY (`unit_definition_bounds`) REFERENCES `UnitDefinition` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_ReactionInOrg_BioSource` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ReactionInOrg_Reaction` FOREIGN KEY (`id_reaction`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13169446 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReactionInNetworkMetadata`
--

DROP TABLE IF EXISTS `ReactionInNetworkMetadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReactionInNetworkMetadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reaction_in_network` int(11) NOT NULL,
  `metadata` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idReactionInBioSource` (`id_reaction_in_network`),
  CONSTRAINT `ReactionInNetworkMetadata_ibfk_1` FOREIGN KEY (`id_reaction_in_network`) REFERENCES `ReactionInNetwork` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2346251 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReactionInPathway`
--

DROP TABLE IF EXISTS `ReactionInPathway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReactionInPathway` (
  `id_pathway` int(11) NOT NULL,
  `id_reaction_in_network` int(11) DEFAULT NULL,
  KEY `fk_ReactionInPathway_Pathway` (`id_pathway`),
  KEY `fk_ReactionInPathway_ReactionInBioSource` (`id_reaction_in_network`),
  CONSTRAINT `fk_ReactionInPathway_Pathway` FOREIGN KEY (`id_pathway`) REFERENCES `Pathway` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ReactionInPathway_ReactionInBioSource` FOREIGN KEY (`id_reaction_in_network`) REFERENCES `ReactionInNetwork` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SavedViz`
--

DROP TABLE IF EXISTS `SavedViz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SavedViz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `json` longtext NOT NULL CHECK (json_valid(`json`)),
  `date_creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modif` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `public` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Status`
--

DROP TABLE IF EXISTS `Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `id_status_type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Status_StatusType1` (`id_status_type`),
  CONSTRAINT `fk_Status_StatusType1` FOREIGN KEY (`id_status_type`) REFERENCES `StatusType` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StatusType`
--

DROP TABLE IF EXISTS `StatusType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StatusType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SuperPathway`
--

DROP TABLE IF EXISTS `SuperPathway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SuperPathway` (
  `id_super_pathway` int(11) NOT NULL,
  `id_pathway` int(11) NOT NULL,
  KEY `fk_SuperPathway_Pathway` (`id_super_pathway`),
  KEY `fk_SuperPathway_Pathway1` (`id_pathway`),
  CONSTRAINT `fk_SuperPathway_Pathway` FOREIGN KEY (`id_super_pathway`) REFERENCES `Pathway` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_SuperPathway_Pathway1` FOREIGN KEY (`id_pathway`) REFERENCES `Pathway` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Synonyms`
--

DROP TABLE IF EXISTS `Synonyms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Synonyms` (
  `other_id` int(11) NOT NULL,
  `synonym` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`synonym`),
  KEY `fk_Synonyms_Metabolite` (`other_id`),
  KEY `fk_Synonyms_Enzyme` (`other_id`),
  KEY `fk_Synonyms_Reaction` (`other_id`),
  CONSTRAINT `fk_Synonyms_Enzyme` FOREIGN KEY (`other_id`) REFERENCES `Enzyme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Synonyms_Metabolite` FOREIGN KEY (`other_id`) REFERENCES `Metabolite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Synonyms_Reaction` FOREIGN KEY (`other_id`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Tokens`
--

DROP TABLE IF EXISTS `Tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `token` varchar(500) NOT NULL,
  `date_expiration` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`id_user`),
  CONSTRAINT `Tokens_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `UserMetExplore3` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Unit`
--

DROP TABLE IF EXISTS `Unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` varchar(45) NOT NULL,
  `exponent` double DEFAULT NULL,
  `scale` int(11) DEFAULT NULL,
  `multiplier` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3466 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UnitDefinition`
--

DROP TABLE IF EXISTS `UnitDefinition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UnitDefinition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL DEFAULT '""',
  `id_collection` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foreign_key_unit_idDatabaseRef` (`id_collection`),
  CONSTRAINT `fk_foreign_key_unit_idDatabaseRef` FOREIGN KEY (`id_collection`) REFERENCES `Collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UnitDefinitionInNetwork`
--

DROP TABLE IF EXISTS `UnitDefinitionInNetwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UnitDefinitionInNetwork` (
  `id_unit_definition` int(11) NOT NULL,
  `id_network` int(11) NOT NULL,
  KEY `fk_foreign_key_unit_idBioSource` (`id_network`),
  KEY `fk_foreign_key_unit_idUnit` (`id_unit_definition`),
  CONSTRAINT `fk_foreign_key_unit_idBioSource` FOREIGN KEY (`id_network`) REFERENCES `Network` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_foreign_key_unit_idUnit` FOREIGN KEY (`id_unit_definition`) REFERENCES `UnitDefinition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserCollection`
--

DROP TABLE IF EXISTS `UserCollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserCollection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission` enum('owner','reader','writer','annotator','no') NOT NULL DEFAULT 'no',
  `id_user` int(11) NOT NULL,
  `id_collection` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foreign_idUser` (`id_user`),
  KEY `fk_foreign_idDatabaseRef` (`id_collection`),
  CONSTRAINT `fk_foreign_idDatabaseRef` FOREIGN KEY (`id_collection`) REFERENCES `Collection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_foreign_idUser` FOREIGN KEY (`id_user`) REFERENCES `UserMetExplore3` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67232 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserInProject`
--

DROP TABLE IF EXISTS `UserInProject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInProject` (
  `id_user` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `id_access` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `notified` tinyint(1) NOT NULL DEFAULT 0,
  `date_last_visit` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_user`,`id_project`),
  KEY `fk_UserInProject_Project1_idx` (`id_project`),
  KEY `fk_UserInProject_Status1_idx` (`id_access`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserMetExplore3`
--

DROP TABLE IF EXISTS `UserMetExplore3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserMetExplore3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(200) NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(45) NOT NULL,
  `last_visit_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `registration_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `max_saved_viz` int(11) NOT NULL DEFAULT 20,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=88266 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='MetExplore 3 User Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `UserNetworkView`
--

DROP TABLE IF EXISTS `UserNetworkView`;
/*!50001 DROP VIEW IF EXISTS `UserNetworkView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `UserNetworkView` AS SELECT
 1 AS `id_user`,
  1 AS `permission`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `UserSavedViz`
--

DROP TABLE IF EXISTS `UserSavedViz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSavedViz` (
  `id_user` int(11) NOT NULL,
  `id_saved_viz` int(11) NOT NULL,
  `permission` enum('owner','reader','writer','annotator','no') NOT NULL DEFAULT 'no',
  KEY `SavedVizConstraint1` (`id_user`),
  KEY `SavedVizConstraint2` (`id_saved_viz`),
  CONSTRAINT `SavedVizConstraint1` FOREIGN KEY (`id_user`) REFERENCES `UserMetExplore3` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SavedVizConstraint2` FOREIGN KEY (`id_saved_viz`) REFERENCES `SavedViz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `ViewReactionEquation`
--

DROP TABLE IF EXISTS `ViewReactionEquation`;
/*!50001 DROP VIEW IF EXISTS `ViewReactionEquation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ViewReactionEquation` AS SELECT
 1 AS `id`,
  1 AS `db_identifier`,
  1 AS `left_side`,
  1 AS `right_side` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `VoteInEnzyme`
--

DROP TABLE IF EXISTS `VoteInEnzyme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoteInEnzyme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_enzyme` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_vote` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_VoteInEnzyme_Enzyme1_idx` (`id_enzyme`),
  KEY `fk_VoteInEnzyme_Status1_idx` (`id_vote`),
  CONSTRAINT `fk_VoteInEnzyme_Enzyme1` FOREIGN KEY (`id_enzyme`) REFERENCES `Enzyme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_VoteInEnzyme_Status1` FOREIGN KEY (`id_vote`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoteInGene`
--

DROP TABLE IF EXISTS `VoteInGene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoteInGene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_gene` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_vote` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_VoteInGene_Gene1_idx` (`id_gene`),
  KEY `fk_VoteInGene_Status1_idx` (`id_vote`),
  CONSTRAINT `fk_VoteInGene_Gene1` FOREIGN KEY (`id_gene`) REFERENCES `Gene` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_VoteInGene_Status1` FOREIGN KEY (`id_vote`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoteInMetabolite`
--

DROP TABLE IF EXISTS `VoteInMetabolite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoteInMetabolite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_metabolite` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_vote` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_VoteInMetabolite_Metabolite1_idx` (`id_metabolite`),
  KEY `fk_VoteInMetabolite_Status1_idx` (`id_vote`),
  CONSTRAINT `fk_VoteInMetabolite_Metabolite1` FOREIGN KEY (`id_metabolite`) REFERENCES `Metabolite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_VoteInMetabolite_Status1` FOREIGN KEY (`id_vote`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoteInPathway`
--

DROP TABLE IF EXISTS `VoteInPathway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoteInPathway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pathway` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_vote` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_VoteInPathway_Pathway1_idx` (`id_pathway`),
  KEY `fk_VoteInPathway_Status1_idx` (`id_vote`),
  CONSTRAINT `fk_VoteInPathway_Pathway1` FOREIGN KEY (`id_pathway`) REFERENCES `Pathway` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_VoteInPathway_Status1` FOREIGN KEY (`id_vote`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1718 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoteInProtein`
--

DROP TABLE IF EXISTS `VoteInProtein`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoteInProtein` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protein` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_vote` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_VoteInProtein_Protein1_idx` (`id_protein`),
  KEY `fk_VoteInProtein_Status1_idx` (`id_vote`),
  CONSTRAINT `fk_VoteInProtein_Protein1` FOREIGN KEY (`id_protein`) REFERENCES `Protein` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_VoteInProtein_Status1` FOREIGN KEY (`id_vote`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoteInReaction`
--

DROP TABLE IF EXISTS `VoteInReaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoteInReaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reaction` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_vote` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_VoteInReaction_Reaction1_idx` (`id_reaction`),
  KEY `fk_VoteInReaction_Status1_idx` (`id_vote`),
  CONSTRAINT `fk_VoteInReaction_Reaction1` FOREIGN KEY (`id_reaction`) REFERENCES `Reaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_VoteInReaction_Status1` FOREIGN KEY (`id_vote`) REFERENCES `Status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_compartment_enzyme`
--

DROP TABLE IF EXISTS `view_compartment_enzyme`;
/*!50001 DROP VIEW IF EXISTS `view_compartment_enzyme`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_compartment_enzyme` AS SELECT
 1 AS `id_enzyme`,
  1 AS `db_identifier_enzyme`,
  1 AS `name_enzyme`,
  1 AS `db_identifier_compartment`,
  1 AS `name_compartment`,
  1 AS `id_compartment`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_compartment_gene`
--

DROP TABLE IF EXISTS `view_compartment_gene`;
/*!50001 DROP VIEW IF EXISTS `view_compartment_gene`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_compartment_gene` AS SELECT
 1 AS `id_gene`,
  1 AS `db_identifier_gene`,
  1 AS `name_gene`,
  1 AS `db_identifier_compartment`,
  1 AS `name_compartment`,
  1 AS `id_compartment`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_compartment_metabolite`
--

DROP TABLE IF EXISTS `view_compartment_metabolite`;
/*!50001 DROP VIEW IF EXISTS `view_compartment_metabolite`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_compartment_metabolite` AS SELECT
 1 AS `id_metabolite`,
  1 AS `db_identifier_metabolite`,
  1 AS `name_compartment`,
  1 AS `db_identifier_compartment` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_compartment_pathway`
--

DROP TABLE IF EXISTS `view_compartment_pathway`;
/*!50001 DROP VIEW IF EXISTS `view_compartment_pathway`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_compartment_pathway` AS SELECT
 1 AS `id_pathway`,
  1 AS `db_identifier_pathway`,
  1 AS `name_pathway`,
  1 AS `db_identifier_compartment`,
  1 AS `name_compartment`,
  1 AS `id_compartment`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_compartment_protein`
--

DROP TABLE IF EXISTS `view_compartment_protein`;
/*!50001 DROP VIEW IF EXISTS `view_compartment_protein`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_compartment_protein` AS SELECT
 1 AS `id_protein`,
  1 AS `db_identifier_protein`,
  1 AS `name_protein`,
  1 AS `db_identifier_compartment`,
  1 AS `name_compartment`,
  1 AS `id_compartment`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_compartment_reaction`
--

DROP TABLE IF EXISTS `view_compartment_reaction`;
/*!50001 DROP VIEW IF EXISTS `view_compartment_reaction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_compartment_reaction` AS SELECT
 1 AS `id_reaction`,
  1 AS `db_identifier_reaction`,
  1 AS `name_reaction`,
  1 AS `db_identifier_compartment`,
  1 AS `name_compartment`,
  1 AS `id_compartment`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_enzyme_gene`
--

DROP TABLE IF EXISTS `view_enzyme_gene`;
/*!50001 DROP VIEW IF EXISTS `view_enzyme_gene`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_enzyme_gene` AS SELECT
 1 AS `id_enzyme`,
  1 AS `db_identifier_enzyme`,
  1 AS `name_enzyme`,
  1 AS `db_identifier_gene`,
  1 AS `name_gene`,
  1 AS `id_gene`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_enzyme_protein`
--

DROP TABLE IF EXISTS `view_enzyme_protein`;
/*!50001 DROP VIEW IF EXISTS `view_enzyme_protein`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_enzyme_protein` AS SELECT
 1 AS `id_enzyme`,
  1 AS `db_identifier_enzyme`,
  1 AS `name_enzyme`,
  1 AS `db_identifier_protein`,
  1 AS `name_protein`,
  1 AS `id_protein`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_metabolite_enzyme`
--

DROP TABLE IF EXISTS `view_metabolite_enzyme`;
/*!50001 DROP VIEW IF EXISTS `view_metabolite_enzyme`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_metabolite_enzyme` AS SELECT
 1 AS `id_metabolite`,
  1 AS `db_identifier_metabolite`,
  1 AS `name_metabolite`,
  1 AS `db_identifier_enzyme`,
  1 AS `name_enzyme`,
  1 AS `id_enzyme`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_metabolite_gene`
--

DROP TABLE IF EXISTS `view_metabolite_gene`;
/*!50001 DROP VIEW IF EXISTS `view_metabolite_gene`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_metabolite_gene` AS SELECT
 1 AS `id_metabolite`,
  1 AS `db_identifier_metabolite`,
  1 AS `name_metabolite`,
  1 AS `db_identifier_gene`,
  1 AS `name_gene`,
  1 AS `id_gene`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_metabolite_protein`
--

DROP TABLE IF EXISTS `view_metabolite_protein`;
/*!50001 DROP VIEW IF EXISTS `view_metabolite_protein`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_metabolite_protein` AS SELECT
 1 AS `id_metabolite`,
  1 AS `db_identifier_metabolite`,
  1 AS `name_metabolite`,
  1 AS `db_identifier_protein`,
  1 AS `name_protein`,
  1 AS `id_protein`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_pathway_enzyme`
--

DROP TABLE IF EXISTS `view_pathway_enzyme`;
/*!50001 DROP VIEW IF EXISTS `view_pathway_enzyme`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_pathway_enzyme` AS SELECT
 1 AS `id_pathway`,
  1 AS `db_identifier_pathway`,
  1 AS `name_pathway`,
  1 AS `db_identifier_enzyme`,
  1 AS `name_enzyme`,
  1 AS `id_enzyme`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_pathway_gene`
--

DROP TABLE IF EXISTS `view_pathway_gene`;
/*!50001 DROP VIEW IF EXISTS `view_pathway_gene`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_pathway_gene` AS SELECT
 1 AS `id_pathway`,
  1 AS `db_identifier_pathway`,
  1 AS `name_pathway`,
  1 AS `db_identifier_gene`,
  1 AS `name_gene`,
  1 AS `id_gene`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_pathway_metabolite`
--

DROP TABLE IF EXISTS `view_pathway_metabolite`;
/*!50001 DROP VIEW IF EXISTS `view_pathway_metabolite`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_pathway_metabolite` AS SELECT
 1 AS `id_pathway`,
  1 AS `db_identifier_pathway`,
  1 AS `name_pathway`,
  1 AS `db_identifier_metabolite`,
  1 AS `name_metabolite`,
  1 AS `id_metabolite`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_pathway_protein`
--

DROP TABLE IF EXISTS `view_pathway_protein`;
/*!50001 DROP VIEW IF EXISTS `view_pathway_protein`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_pathway_protein` AS SELECT
 1 AS `id_pathway`,
  1 AS `db_identifier_pathway`,
  1 AS `name_pathway`,
  1 AS `db_identifier_protein`,
  1 AS `name_protein`,
  1 AS `id_protein`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_protein_gene`
--

DROP TABLE IF EXISTS `view_protein_gene`;
/*!50001 DROP VIEW IF EXISTS `view_protein_gene`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_protein_gene` AS SELECT
 1 AS `id_protein`,
  1 AS `db_identifier_protein`,
  1 AS `name_protein`,
  1 AS `db_identifier_gene`,
  1 AS `name_gene`,
  1 AS `id_gene`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_reaction_enzyme`
--

DROP TABLE IF EXISTS `view_reaction_enzyme`;
/*!50001 DROP VIEW IF EXISTS `view_reaction_enzyme`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_reaction_enzyme` AS SELECT
 1 AS `id_reaction`,
  1 AS `db_identifier_reaction`,
  1 AS `name_reaction`,
  1 AS `db_identifier_enzyme`,
  1 AS `name_enzyme`,
  1 AS `id_enzyme`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_reaction_equation`
--

DROP TABLE IF EXISTS `view_reaction_equation`;
/*!50001 DROP VIEW IF EXISTS `view_reaction_equation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_reaction_equation` AS SELECT
 1 AS `id_network`,
  1 AS `db_identifier`,
  1 AS `date_modification`,
  1 AS `id`,
  1 AS `name`,
  1 AS `ec`,
  1 AS `reversible`,
  1 AS `eq_identifier`,
  1 AS `eq_name` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_reaction_gene`
--

DROP TABLE IF EXISTS `view_reaction_gene`;
/*!50001 DROP VIEW IF EXISTS `view_reaction_gene`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_reaction_gene` AS SELECT
 1 AS `id_reaction`,
  1 AS `db_identifier_reaction`,
  1 AS `name_reaction`,
  1 AS `db_identifier_gene`,
  1 AS `name_gene`,
  1 AS `id_gene`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_reaction_metabolite`
--

DROP TABLE IF EXISTS `view_reaction_metabolite`;
/*!50001 DROP VIEW IF EXISTS `view_reaction_metabolite`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_reaction_metabolite` AS SELECT
 1 AS `id_reaction`,
  1 AS `db_identifier_reaction`,
  1 AS `name_reaction`,
  1 AS `db_identifier_metabolite`,
  1 AS `name_metabolite`,
  1 AS `id_metabolite`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_reaction_protein`
--

DROP TABLE IF EXISTS `view_reaction_protein`;
/*!50001 DROP VIEW IF EXISTS `view_reaction_protein`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_reaction_protein` AS SELECT
 1 AS `id_reaction`,
  1 AS `db_identifier_reaction`,
  1 AS `name_reaction`,
  1 AS `db_identifier_protein`,
  1 AS `name_protein`,
  1 AS `id_protein`,
  1 AS `id_network` */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_stat`
--

DROP TABLE IF EXISTS `view_stat`;
/*!50001 DROP VIEW IF EXISTS `view_stat`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_stat` AS SELECT
 1 AS `id_network`,
  1 AS `biodata`,
  1 AS `nb` */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'metexplore_test'
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `admin_getLinkedTables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`metexplore`@`localhost` PROCEDURE `admin_getLinkedTables`(IN `tableIn` VARCHAR(45))
    SQL SECURITY INVOKER
    COMMENT 'Affiche toutes les tables qui ont une clÃ© Ã©trangÃ¨re dans tableIn'
SELECT DISTINCT TABLE_NAME

FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE

WHERE REFERENCED_TABLE_NAME = tableIn

  AND REFERENCED_COLUMN_NAME IN (

    SELECT COLUMN_NAME

    FROM INFORMATION_SCHEMA.COLUMNS

    WHERE TABLE_NAME = tableIn

  ) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `analysisData_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`metexplore`@`localhost` PROCEDURE `analysisData_add`(IN `analysisData_name` VARCHAR(100), IN `analysisData_description` TEXT, IN `analysisData_type` ENUM('image','datafile','dataset','plot','viz','other'), IN `analysisData_json` LONGTEXT, IN `step_id` INT, OUT `id_analysisData` INT(100))
    SQL SECURITY INVOKER
BEGIN





DECLARE EXIT HANDLER FOR SQLEXCEPTION

BEGIN

	ROLLBACK;

END;



START TRANSACTION;

    

	INSERT INTO AnalysisData (name, description, type, json) VALUES (analysisData_name, analysisData_description, analysisData_type, analysisData_json);

	

	SET @id_analysisData:=LAST_INSERT_ID();

    

    INSERT INTO AnalysisDataStep (id_data, id_step) VALUES (@id_analysisData, step_id); 



	SELECT @id_analysisData;

    

COMMIT;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `analysisData_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`metexplore`@`localhost` PROCEDURE `analysisData_delete`(IN `analysisData_id` INT)
    SQL SECURITY INVOKER
BEGIN





DECLARE EXIT HANDLER FOR SQLEXCEPTION

BEGIN

	ROLLBACK;

END;





DELETE FROM AnalysisDataStep WHERE id_data=analysisData_id;



DELETE FROM AnalysisData WHERE id=analysisData_id;



COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `analysisMapping_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`metexplore`@`localhost` PROCEDURE `analysisMapping_add`(IN `analysis_name` VARCHAR(100), IN `analysis_description` VARCHAR(255), IN `analysis_url` VARCHAR(100), IN `user_token` VARCHAR(500), IN `mapping_input` LONGTEXT, IN `mapping_output` LONGTEXT, OUT `id_analysis` INT(100))
    SQL SECURITY INVOKER
BEGIN



DECLARE EXIT HANDLER FOR SQLEXCEPTION

BEGIN

	ROLLBACK;

END;



START TRANSACTION;

    

	CALL analysis_add(analysis_name, analysis_description, analysis_url, user_token);

  SET @id_analysis:=(SELECT id FROM Analysis ORDER BY id DESC LIMIT 1);

    

    INSERT INTO `AnalysisStep` (`name`, `description`, `id_analysis`, `type`, `root`)  VALUES ('input', '', @id_analysis, 'data', '1');

	SET @first_id_analysis_step:=LAST_INSERT_ID();

    

    CALL analysisData_add('input', '', 'dataset', mapping_input, @first_id_analysis_step, @id_analysisData);

    

    INSERT INTO `AnalysisStep` (`name`, `description`, `id_analysis`, `type`, `root`) VALUES ('mapping', '', @id_analysis, 'job', '0');

    SET @second_id_analysis_step:=LAST_INSERT_ID();

    

    INSERT INTO `AnalysisStepLink` (`id_source`, `id_target`, `type`) VALUES (@first_id_analysis_step, @second_id_analysis_step, '');

    

    INSERT INTO `AnalysisJobStep` (`id_app`, `id_step`, `parameters`) VALUES (3, @second_id_analysis_step, '');

    

    INSERT INTO `AnalysisStep` (`name`, `description`, `id_analysis`, `type`, `root`) VALUES ('output', '', @id_analysis, 'data', '0');

	SET @third_id_analysis_step:=LAST_INSERT_ID();

    

    CALL analysisData_add('output', '', 'dataset', mapping_output, @third_id_analysis_step, @id_analysisData);

    

    INSERT INTO `AnalysisStepLink` (`id_source`, `id_target`, `type`) VALUES (@second_id_analysis_step, @third_id_analysis_step, '');

    

    SELECT @id_analysis;

    

COMMIT;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `analysis_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`metexplore`@`localhost` PROCEDURE `analysis_add`(IN `analysis_name` VARCHAR(100), IN `analysis_description` TEXT, IN `analysis_url` VARCHAR(100), IN `user_token` VARCHAR(500))
    SQL SECURITY INVOKER
    COMMENT 'ajoute une analyse liÃ©e Ã  un user en tant que ''admin'''
BEGIN





DECLARE EXIT HANDLER FOR SQLEXCEPTION

BEGIN

	ROLLBACK;

END;



START TRANSACTION;

    

	INSERT INTO Analysis (name, description, url) VALUES (analysis_name, analysis_description, analysis_url);

	

	SET @id_analysis:=LAST_INSERT_ID();

	

	SET @id_user = (SELECT id_user FROM Tokens WHERE token=user_token);

	

	INSERT INTO AnalysisUserRights (id_user, id_analysis, mode) VALUES (@id_user, @id_analysis, 'admin');

    

    SELECT @id_analysis as id_analysis;

    

COMMIT;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `analysis_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`metexplore`@`localhost` PROCEDURE `analysis_delete`(IN `analysis_id` INT, IN `user_token` VARCHAR(500))
    SQL SECURITY INVOKER
    COMMENT 'supprime une analyse '
BEGIN



DECLARE EXIT HANDLER FOR SQLEXCEPTION

BEGIN

	ROLLBACK;

END;





SELECT @id_user:=id_user FROM Tokens WHERE token=user_token;



SELECT @mode:=mode FROM AnalysisUserRights WHERE id_analysis=analysis_id AND id_user=@id_user;



IF @mode = 'admin' THEN

    DELETE FROM AnalysisUserRights WHERE id_analysis=analysis_id;

    DELETE FROM Analysis WHERE id=analysis_id;

ELSE



DELETE FROM AnalysisUserRights WHERE id_analysis=analysis_id AND id_user=@id_user;

END IF;



COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `metexplore_test`
--

USE `metexplore_test`;

--
-- Final view structure for view `GraphEdgesView`
--

/*!50001 DROP VIEW IF EXISTS `GraphEdgesView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `GraphEdgesView` AS select case when `React`.`side` = 'LEFT' then `M`.`db_identifier` else `R`.`db_identifier` end AS `source`,case when `React`.`side` = 'LEFT' then 'substrate-reaction' else 'reaction-product' end AS `interaction`,case when `React`.`side` = 'LEFT' then `R`.`db_identifier` else `M`.`db_identifier` end AS `target`,`RN`.`id_network` AS `id_network`,`RN`.`reversible` AS `reversible` from (((`Reaction` `R` join `Reactant` `React` on(`R`.`id` = `React`.`id_reaction`)) join `Metabolite` `M` on(`React`.`id_metabolite` = `M`.`id`)) join `ReactionInNetwork` `RN` on(`R`.`id` = `RN`.`id_reaction`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `GraphMetaboliteNodesView`
--

/*!50001 DROP VIEW IF EXISTS `GraphMetaboliteNodesView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `GraphMetaboliteNodesView` AS select `mc`.`db_identifier` AS `db_identifier`,`mc`.`name` AS `name`,if(count(`mp`.`pathway_name`) = 0,'',group_concat(distinct `mp`.`pathway_name` separator ',')) AS `pathways`,if(count(`mc`.`compartment_name`) = 0,'',group_concat(distinct `mc`.`compartment_name` separator ',')) AS `compartments`,`ms`.`side_compound` AS `side_compound`,`mc`.`id_network` AS `id_network` from ((`MetaboliteCompartmentsView` `mc` join `MetaboliteSideCompoundView` `ms` on(`mc`.`db_identifier` = `ms`.`db_identifier`)) left join `MetabolitePathwaysView` `mp` on(`mc`.`db_identifier` = `mp`.`db_identifier`)) group by `mc`.`db_identifier`,`mc`.`name`,`ms`.`side_compound`,`mc`.`id_network` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `GraphReactionNodesView`
--

/*!50001 DROP VIEW IF EXISTS `GraphReactionNodesView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `GraphReactionNodesView` AS select `Reaction`.`db_identifier` AS `db_identifier`,`Reaction`.`name` AS `name`,if(count(`Pathway`.`name`) = 0,'',group_concat(`Pathway`.`name` separator ',')) AS `pathways`,`ReactionInNetwork`.`id_network` AS `id_network`,`ReactionInNetwork`.`reversible` AS `reversible` from (((`Reaction` left join `ReactionInNetwork` on(`Reaction`.`id` = `ReactionInNetwork`.`id_reaction`)) left join `ReactionInPathway` on(`ReactionInNetwork`.`id` = `ReactionInPathway`.`id_reaction_in_network`)) left join `Pathway` on(`ReactionInPathway`.`id_pathway` = `Pathway`.`id`)) group by `Reaction`.`db_identifier`,`Reaction`.`name`,`ReactionInNetwork`.`id_network`,`ReactionInNetwork`.`reversible` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `MetaboliteCompartmentsView`
--

/*!50001 DROP VIEW IF EXISTS `MetaboliteCompartmentsView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `MetaboliteCompartmentsView` AS select `m`.`db_identifier` AS `db_identifier`,`m`.`name` AS `name`,`c`.`name` AS `compartment_name`,`min`.`id_network` AS `id_network` from (((`Metabolite` `m` join `MetaboliteInNetwork` `min` on(`m`.`id` = `min`.`id_metabolite`)) join `MetaboliteInCompartment` `mic` on(`min`.`id` = `mic`.`id_metabolite_in_network`)) join `Compartment` `c` on(`mic`.`id_compartment` = `c`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `MetabolitePathwaysView`
--

/*!50001 DROP VIEW IF EXISTS `MetabolitePathwaysView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `MetabolitePathwaysView` AS select `m`.`db_identifier` AS `db_identifier`,`m`.`name` AS `name`,`p`.`name` AS `pathway_name`,`min`.`id_network` AS `id_network` from (((((`Metabolite` `m` join `MetaboliteInNetwork` `min` on(`m`.`id` = `min`.`id_metabolite`)) join `Reactant` `r` on(`m`.`id` = `r`.`id_metabolite`)) join `ReactionInNetwork` `rin` on(`r`.`id_reaction` = `rin`.`id_reaction`)) join `ReactionInPathway` `rip` on(`rin`.`id` = `rip`.`id_reaction_in_network`)) join `Pathway` `p` on(`rip`.`id_pathway` = `p`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `MetaboliteSideCompoundView`
--

/*!50001 DROP VIEW IF EXISTS `MetaboliteSideCompoundView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `MetaboliteSideCompoundView` AS select `Metabolite`.`db_identifier` AS `db_identifier`,`MetaboliteInNetwork`.`side_compound` AS `side_compound`,`MetaboliteInNetwork`.`id_network` AS `id_network` from (`Metabolite` join `MetaboliteInNetwork` on(`MetaboliteInNetwork`.`id_metabolite` = `Metabolite`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `PathwayMetabolitesView`
--

/*!50001 DROP VIEW IF EXISTS `PathwayMetabolitesView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `PathwayMetabolitesView` AS select distinct `Pathway`.`db_identifier` AS `pathway_db_identifier`,`Pathway`.`name` AS `pathway_name`,`Pathway`.`id_network` AS `id_network`,`Metabolite`.`db_identifier` AS `metabolite_db_identifier` from ((((`Pathway` join `ReactionInPathway` on(`Pathway`.`id` = `ReactionInPathway`.`id_pathway`)) join `ReactionInNetwork` on(`ReactionInPathway`.`id_reaction_in_network` = `ReactionInNetwork`.`id`)) join `Reactant` on(`ReactionInNetwork`.`id_reaction` = `Reactant`.`id_reaction`)) join `Metabolite` on(`Reactant`.`id_metabolite` = `Metabolite`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `PathwayReactionsView`
--

/*!50001 DROP VIEW IF EXISTS `PathwayReactionsView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `PathwayReactionsView` AS select distinct `Pathway`.`db_identifier` AS `pathway_db_identifier`,`Pathway`.`name` AS `pathway_name`,`Pathway`.`id_network` AS `id_network`,`Reaction`.`db_identifier` AS `reaction_db_identifier` from (((`Pathway` join `ReactionInPathway` on(`Pathway`.`id` = `ReactionInPathway`.`id_pathway`)) join `ReactionInNetwork` on(`ReactionInPathway`.`id_reaction_in_network` = `ReactionInNetwork`.`id`)) join `Reaction` on(`ReactionInNetwork`.`id_reaction` = `Reaction`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `UserNetworkView`
--

/*!50001 DROP VIEW IF EXISTS `UserNetworkView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `UserNetworkView` AS select `UserCollection`.`id_user` AS `id_user`,`UserCollection`.`permission` AS `permission`,`Network`.`id` AS `id_network` from (`Network` join `UserCollection`) where `UserCollection`.`id_collection` = `Network`.`id_collection` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ViewReactionEquation`
--

/*!50001 DROP VIEW IF EXISTS `ViewReactionEquation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ViewReactionEquation` AS select distinct `Reaction`.`id` AS `id`,`Reaction`.`db_identifier` AS `db_identifier`,group_concat(case when `r`.`side` = 'LEFT' then concat(`r`.`coeff`,' ',`m`.`db_identifier`) else NULL end separator ' + ') AS `left_side`,group_concat(case when `r`.`side` = 'RIGHT' then concat(`r`.`coeff`,' ',`m`.`db_identifier`) else NULL end separator ' + ') AS `right_side` from ((`Reaction` join `Reactant` `r`) join `Metabolite` `m`) where `Reaction`.`id` = `r`.`id_reaction` and `r`.`id_metabolite` = `m`.`id` group by `Reaction`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_compartment_enzyme`
--

/*!50001 DROP VIEW IF EXISTS `view_compartment_enzyme`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_compartment_enzyme` AS select distinct `Enzyme`.`id` AS `id_enzyme`,`Enzyme`.`db_identifier` AS `db_identifier_enzyme`,`Enzyme`.`name` AS `name_enzyme`,`Compartment`.`db_identifier` AS `db_identifier_compartment`,`Compartment`.`name` AS `name_compartment`,`Compartment`.`id` AS `id_compartment`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((((`Enzyme` join `Catalyses`) join `ReactionInNetwork`) join `Reaction`) join `Reactant`) join `Metabolite`) join `MetaboliteInNetwork`) join `MetaboliteInCompartment`) join `Compartment`) where `Enzyme`.`id` = `Catalyses`.`id_enzyme` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Reaction`.`id` = `Reactant`.`id_reaction` and `Reactant`.`id_metabolite` = `Metabolite`.`id` and `Metabolite`.`id` = `MetaboliteInNetwork`.`id_metabolite` and `MetaboliteInNetwork`.`id` = `MetaboliteInCompartment`.`id_metabolite_in_network` and `MetaboliteInCompartment`.`id_compartment` = `Compartment`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_compartment_gene`
--

/*!50001 DROP VIEW IF EXISTS `view_compartment_gene`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_compartment_gene` AS select distinct `Gene`.`id` AS `id_gene`,`Gene`.`db_identifier` AS `db_identifier_gene`,`Gene`.`name` AS `name_gene`,`Compartment`.`db_identifier` AS `db_identifier_compartment`,`Compartment`.`name` AS `name_compartment`,`Compartment`.`id` AS `id_compartment`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((((((((`Gene` join `GeneCodesForProtein`) join `Protein`) join `ProteinInEnzyme`) join `Enzyme`) join `Catalyses`) join `ReactionInNetwork`) join `Reaction`) join `Reactant`) join `Metabolite`) join `MetaboliteInNetwork`) join `MetaboliteInCompartment`) join `Compartment`) where `Gene`.`id` = `GeneCodesForProtein`.`id_gene` and `GeneCodesForProtein`.`id_protein` = `Protein`.`id` and `Protein`.`id` = `ProteinInEnzyme`.`id_protein` and `ProteinInEnzyme`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `Catalyses`.`id_enzyme` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Reaction`.`id` = `Reactant`.`id_reaction` and `Reactant`.`id_metabolite` = `Metabolite`.`id` and `Metabolite`.`id` = `MetaboliteInNetwork`.`id_metabolite` and `MetaboliteInNetwork`.`id` = `MetaboliteInCompartment`.`id_metabolite_in_network` and `MetaboliteInCompartment`.`id_compartment` = `Compartment`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_compartment_metabolite`
--

/*!50001 DROP VIEW IF EXISTS `view_compartment_metabolite`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_compartment_metabolite` AS select distinct `Metabolite`.`id` AS `id_metabolite`,`Metabolite`.`db_identifier` AS `db_identifier_metabolite`,`Compartment`.`name` AS `name_compartment`,`Compartment`.`db_identifier` AS `db_identifier_compartment` from (((`Metabolite` join `MetaboliteInNetwork`) join `MetaboliteInCompartment`) join `Compartment`) where `Metabolite`.`id` = `MetaboliteInNetwork`.`id_metabolite` and `MetaboliteInNetwork`.`id` = `MetaboliteInCompartment`.`id_metabolite_in_network` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_compartment_pathway`
--

/*!50001 DROP VIEW IF EXISTS `view_compartment_pathway`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_compartment_pathway` AS select distinct `Pathway`.`id` AS `id_pathway`,`Pathway`.`db_identifier` AS `db_identifier_pathway`,`Pathway`.`name` AS `name_pathway`,`Compartment`.`db_identifier` AS `db_identifier_compartment`,`Compartment`.`name` AS `name_compartment`,`Compartment`.`id` AS `id_compartment`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((((`Pathway` join `ReactionInPathway`) join `ReactionInNetwork`) join `Reaction`) join `Reactant`) join `Metabolite`) join `MetaboliteInNetwork`) join `MetaboliteInCompartment`) join `Compartment`) where `Pathway`.`id` = `ReactionInPathway`.`id_pathway` and `ReactionInPathway`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Reaction`.`id` = `Reactant`.`id_reaction` and `Reactant`.`id_metabolite` = `Metabolite`.`id` and `Metabolite`.`id` = `MetaboliteInNetwork`.`id_metabolite` and `MetaboliteInNetwork`.`id` = `MetaboliteInCompartment`.`id_metabolite_in_network` and `MetaboliteInCompartment`.`id_compartment` = `Compartment`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_compartment_protein`
--

/*!50001 DROP VIEW IF EXISTS `view_compartment_protein`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_compartment_protein` AS select distinct `Protein`.`id` AS `id_protein`,`Protein`.`db_identifier` AS `db_identifier_protein`,`Protein`.`name` AS `name_protein`,`Compartment`.`db_identifier` AS `db_identifier_compartment`,`Compartment`.`name` AS `name_compartment`,`Compartment`.`id` AS `id_compartment`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((((((`Protein` join `ProteinInEnzyme`) join `Enzyme`) join `Catalyses`) join `ReactionInNetwork`) join `Reaction`) join `Reactant`) join `Metabolite`) join `MetaboliteInNetwork`) join `MetaboliteInCompartment`) join `Compartment`) where `Protein`.`id` = `ProteinInEnzyme`.`id_protein` and `ProteinInEnzyme`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `Catalyses`.`id_enzyme` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Reaction`.`id` = `Reactant`.`id_reaction` and `Reactant`.`id_metabolite` = `Metabolite`.`id` and `Metabolite`.`id` = `MetaboliteInNetwork`.`id_metabolite` and `MetaboliteInNetwork`.`id` = `MetaboliteInCompartment`.`id_metabolite_in_network` and `MetaboliteInCompartment`.`id_compartment` = `Compartment`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_compartment_reaction`
--

/*!50001 DROP VIEW IF EXISTS `view_compartment_reaction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_compartment_reaction` AS select distinct `Reaction`.`id` AS `id_reaction`,`Reaction`.`db_identifier` AS `db_identifier_reaction`,`Reaction`.`name` AS `name_reaction`,`Compartment`.`db_identifier` AS `db_identifier_compartment`,`Compartment`.`name` AS `name_compartment`,`Compartment`.`id` AS `id_compartment`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((`ReactionInNetwork` join `Reaction`) join `Reactant`) join `Metabolite`) join `MetaboliteInNetwork`) join `MetaboliteInCompartment`) join `Compartment`) where `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Reaction`.`id` = `Reactant`.`id_reaction` and `Reactant`.`id_metabolite` = `Metabolite`.`id` and `Metabolite`.`id` = `MetaboliteInNetwork`.`id_metabolite` and `MetaboliteInNetwork`.`id` = `MetaboliteInCompartment`.`id_metabolite_in_network` and `MetaboliteInCompartment`.`id_compartment` = `Compartment`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_enzyme_gene`
--

/*!50001 DROP VIEW IF EXISTS `view_enzyme_gene`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_enzyme_gene` AS select distinct `Enzyme`.`id` AS `id_enzyme`,`Enzyme`.`db_identifier` AS `db_identifier_enzyme`,`Enzyme`.`name` AS `name_enzyme`,`Gene`.`db_identifier` AS `db_identifier_gene`,`Gene`.`name` AS `name_gene`,`Gene`.`id` AS `id_gene`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((`ReactionInNetwork` join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) join `GeneCodesForProtein`) join `Gene`) where `ReactionInNetwork`.`id` = `Catalyses`.`id_reaction_in_network` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `Protein`.`id` = `ProteinInEnzyme`.`id_protein` and `GeneCodesForProtein`.`id_protein` = `Protein`.`id` and `GeneCodesForProtein`.`id_gene` = `Gene`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_enzyme_protein`
--

/*!50001 DROP VIEW IF EXISTS `view_enzyme_protein`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_enzyme_protein` AS select distinct `Enzyme`.`id` AS `id_enzyme`,`Enzyme`.`db_identifier` AS `db_identifier_enzyme`,`Enzyme`.`name` AS `name_enzyme`,`Protein`.`db_identifier` AS `db_identifier_protein`,`Protein`.`name` AS `name_protein`,`Protein`.`id` AS `id_protein`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((`ReactionInNetwork` join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) where `ReactionInNetwork`.`id` = `Catalyses`.`id_reaction_in_network` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `ProteinInEnzyme`.`id_protein` = `Protein`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_metabolite_enzyme`
--

/*!50001 DROP VIEW IF EXISTS `view_metabolite_enzyme`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_metabolite_enzyme` AS select distinct `Metabolite`.`id` AS `id_metabolite`,`Metabolite`.`db_identifier` AS `db_identifier_metabolite`,`Metabolite`.`name` AS `name_metabolite`,`Enzyme`.`db_identifier` AS `db_identifier_enzyme`,`Enzyme`.`name` AS `name_enzyme`,`Enzyme`.`id` AS `id_enzyme`,`ReactionInNetwork`.`id_network` AS `id_network` from (((((`Metabolite` join `Reactant`) join `Reaction`) join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) where `Metabolite`.`id` = `Reactant`.`id_metabolite` and `Reactant`.`id_reaction` = `Reaction`.`id` and `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `ReactionInNetwork`.`id` = `Catalyses`.`id_reaction_in_network` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_metabolite_gene`
--

/*!50001 DROP VIEW IF EXISTS `view_metabolite_gene`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_metabolite_gene` AS select distinct `Metabolite`.`id` AS `id_metabolite`,`Metabolite`.`db_identifier` AS `db_identifier_metabolite`,`Metabolite`.`name` AS `name_metabolite`,`Gene`.`db_identifier` AS `db_identifier_gene`,`Gene`.`name` AS `name_gene`,`Gene`.`id` AS `id_gene`,`ReactionInNetwork`.`id_network` AS `id_network` from (((((((((`Metabolite` join `Reactant`) join `Reaction`) join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) join `GeneCodesForProtein`) join `Gene`) where `Metabolite`.`id` = `Reactant`.`id_metabolite` and `Reactant`.`id_reaction` = `Reaction`.`id` and `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `ReactionInNetwork`.`id` = `Catalyses`.`id_reaction_in_network` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `Protein`.`id` = `ProteinInEnzyme`.`id_protein` and `GeneCodesForProtein`.`id_protein` = `Protein`.`id` and `GeneCodesForProtein`.`id_gene` = `Gene`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_metabolite_protein`
--

/*!50001 DROP VIEW IF EXISTS `view_metabolite_protein`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_metabolite_protein` AS select distinct `Metabolite`.`id` AS `id_metabolite`,`Metabolite`.`db_identifier` AS `db_identifier_metabolite`,`Metabolite`.`name` AS `name_metabolite`,`Protein`.`db_identifier` AS `db_identifier_protein`,`Protein`.`name` AS `name_protein`,`Protein`.`id` AS `id_protein`,`ReactionInNetwork`.`id_network` AS `id_network` from (((((((`Metabolite` join `Reactant`) join `Reaction`) join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) where `Metabolite`.`id` = `Reactant`.`id_metabolite` and `Reactant`.`id_reaction` = `Reaction`.`id` and `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `ReactionInNetwork`.`id` = `Catalyses`.`id_reaction_in_network` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `ProteinInEnzyme`.`id_protein` = `Protein`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_pathway_enzyme`
--

/*!50001 DROP VIEW IF EXISTS `view_pathway_enzyme`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_pathway_enzyme` AS select distinct `Pathway`.`id` AS `id_pathway`,`Pathway`.`db_identifier` AS `db_identifier_pathway`,`Pathway`.`name` AS `name_pathway`,`Enzyme`.`db_identifier` AS `db_identifier_enzyme`,`Enzyme`.`name` AS `name_enzyme`,`Enzyme`.`id` AS `id_enzyme`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((`Pathway` join `ReactionInPathway`) join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) where `Pathway`.`id` = `ReactionInPathway`.`id_pathway` and `ReactionInPathway`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_pathway_gene`
--

/*!50001 DROP VIEW IF EXISTS `view_pathway_gene`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_pathway_gene` AS select distinct `Pathway`.`id` AS `id_pathway`,`Pathway`.`db_identifier` AS `db_identifier_pathway`,`Pathway`.`name` AS `name_pathway`,`Gene`.`db_identifier` AS `db_identifier_gene`,`Gene`.`name` AS `name_gene`,`Gene`.`id` AS `id_gene`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((((`Pathway` join `ReactionInPathway`) join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) join `GeneCodesForProtein`) join `Gene`) where `Pathway`.`id` = `ReactionInPathway`.`id_pathway` and `ReactionInPathway`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `Protein`.`id` = `ProteinInEnzyme`.`id_protein` and `GeneCodesForProtein`.`id_protein` = `Protein`.`id` and `GeneCodesForProtein`.`id_gene` = `Gene`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_pathway_metabolite`
--

/*!50001 DROP VIEW IF EXISTS `view_pathway_metabolite`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_pathway_metabolite` AS select distinct `Pathway`.`id` AS `id_pathway`,`Pathway`.`db_identifier` AS `db_identifier_pathway`,`Pathway`.`name` AS `name_pathway`,`Metabolite`.`db_identifier` AS `db_identifier_metabolite`,`Metabolite`.`name` AS `name_metabolite`,`Metabolite`.`id` AS `id_metabolite`,`ReactionInNetwork`.`id_network` AS `id_network` from (((((`Pathway` join `ReactionInPathway`) join `ReactionInNetwork`) join `Reactant`) join `Reaction`) join `Metabolite`) where `Pathway`.`id` = `ReactionInPathway`.`id_pathway` and `ReactionInPathway`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `ReactionInNetwork`.`id_reaction` = `Reaction`.`id` and `Reaction`.`id` = `Reactant`.`id_reaction` and `Reactant`.`id_metabolite` = `Metabolite`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_pathway_protein`
--

/*!50001 DROP VIEW IF EXISTS `view_pathway_protein`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_pathway_protein` AS select distinct `Pathway`.`id` AS `id_pathway`,`Pathway`.`db_identifier` AS `db_identifier_pathway`,`Pathway`.`name` AS `name_pathway`,`Protein`.`db_identifier` AS `db_identifier_protein`,`Protein`.`name` AS `name_protein`,`Protein`.`id` AS `id_protein`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((`Pathway` join `ReactionInPathway`) join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) where `Pathway`.`id` = `ReactionInPathway`.`id_pathway` and `ReactionInPathway`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `Protein`.`id` = `ProteinInEnzyme`.`id_protein` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_protein_gene`
--

/*!50001 DROP VIEW IF EXISTS `view_protein_gene`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_protein_gene` AS select distinct `Protein`.`id` AS `id_protein`,`Protein`.`db_identifier` AS `db_identifier_protein`,`Protein`.`name` AS `name_protein`,`Gene`.`db_identifier` AS `db_identifier_gene`,`Gene`.`name` AS `name_gene`,`Gene`.`id` AS `id_gene`,`ReactionInNetwork`.`id_network` AS `id_network` from ((((((`ReactionInNetwork` join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Gene`) join `GeneCodesForProtein`) join `Protein`) where `ReactionInNetwork`.`id` = `Catalyses`.`id_reaction_in_network` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `ProteinInEnzyme`.`id_protein` = `Protein`.`id` and `Gene`.`id` = `GeneCodesForProtein`.`id_gene` and `GeneCodesForProtein`.`id_protein` = `Protein`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_reaction_enzyme`
--

/*!50001 DROP VIEW IF EXISTS `view_reaction_enzyme`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reaction_enzyme` AS select distinct `Reaction`.`id` AS `id_reaction`,`Reaction`.`db_identifier` AS `db_identifier_reaction`,`Reaction`.`name` AS `name_reaction`,`Enzyme`.`db_identifier` AS `db_identifier_enzyme`,`Enzyme`.`name` AS `name_enzyme`,`Enzyme`.`id` AS `id_enzyme`,`ReactionInNetwork`.`id_network` AS `id_network` from (((`Reaction` join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) where `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_reaction_equation`
--

/*!50001 DROP VIEW IF EXISTS `view_reaction_equation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `view_reaction_equation` AS select distinct `ReactionInNetwork`.`id_network` AS `id_network`,`Reaction`.`db_identifier` AS `db_identifier`,`Reaction`.`date_modification` AS `date_modification`,`Reaction`.`id` AS `id`,`Reaction`.`name` AS `name`,`Reaction`.`ec` AS `ec`,`ReactionInNetwork`.`reversible` AS `reversible`,case when `ReactionInNetwork`.`reversible` = 0 then concat(group_concat(case when `r`.`side` = 'LEFT' then concat(`r`.`coeff`,' ',`m`.`db_identifier`) else NULL end separator ' + '),' -> ',group_concat(case when `r`.`side` = 'RIGHT' then concat(`r`.`coeff`,' ',`m`.`db_identifier`) else NULL end separator ' + ')) when `ReactionInNetwork`.`reversible` = 1 then concat(group_concat(case when `r`.`side` = 'LEFT' then concat(`r`.`coeff`,' ',`m`.`db_identifier`) else NULL end separator ' + '),' <-> ',group_concat(case when `r`.`side` = 'RIGHT' then concat(`r`.`coeff`,' ',`m`.`db_identifier`) else NULL end separator ' + ')) end AS `eq_identifier`,case when `ReactionInNetwork`.`reversible` = 0 then concat(group_concat(case when `r`.`side` = 'LEFT' then concat(`r`.`coeff`,' ',`m`.`name`) else NULL end separator ' + '),' -> ',group_concat(case when `r`.`side` = 'RIGHT' then concat(`r`.`coeff`,' ',`m`.`name`) else NULL end separator ' + ')) when `ReactionInNetwork`.`reversible` = 1 then concat(group_concat(case when `r`.`side` = 'LEFT' then concat(`r`.`coeff`,' ',`m`.`name`) else NULL end separator ' + '),' <-> ',group_concat(case when `r`.`side` = 'RIGHT' then concat(`r`.`coeff`,' ',`m`.`name`) else NULL end separator ' + ')) end AS `eq_name` from (((`ReactionInNetwork` join `Reaction` on(`ReactionInNetwork`.`id_reaction` = `Reaction`.`id`)) left join `Reactant` `r` on(`Reaction`.`id` = `r`.`id_reaction`)) left join `Metabolite` `m` on(`r`.`id_metabolite` = `m`.`id`)) group by `Reaction`.`id`,`ReactionInNetwork`.`id_network`,`Reaction`.`db_identifier`,`Reaction`.`name`,`Reaction`.`ec`,`ReactionInNetwork`.`reversible`,`Reaction`.`date_modification` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_reaction_gene`
--

/*!50001 DROP VIEW IF EXISTS `view_reaction_gene`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reaction_gene` AS select distinct `Reaction`.`id` AS `id_reaction`,`Reaction`.`db_identifier` AS `db_identifier_reaction`,`Reaction`.`name` AS `name_reaction`,`Gene`.`db_identifier` AS `db_identifier_gene`,`Gene`.`name` AS `name_gene`,`Gene`.`id` AS `id_gene`,`ReactionInNetwork`.`id_network` AS `id_network` from (((((((`Reaction` join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) join `GeneCodesForProtein`) join `Gene`) where `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `Protein`.`id` = `ProteinInEnzyme`.`id_protein` and `GeneCodesForProtein`.`id_protein` = `Protein`.`id` and `GeneCodesForProtein`.`id_gene` = `Gene`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_reaction_metabolite`
--

/*!50001 DROP VIEW IF EXISTS `view_reaction_metabolite`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reaction_metabolite` AS select distinct `Reaction`.`id` AS `id_reaction`,`Reaction`.`db_identifier` AS `db_identifier_reaction`,`Reaction`.`name` AS `name_reaction`,`Metabolite`.`db_identifier` AS `db_identifier_metabolite`,`Metabolite`.`name` AS `name_metabolite`,`Metabolite`.`id` AS `id_metabolite`,`ReactionInNetwork`.`id_network` AS `id_network` from (((`ReactionInNetwork` join `Reaction`) join `Reactant`) join `Metabolite`) where `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Reaction`.`id` = `Reactant`.`id_reaction` and `Reactant`.`id_metabolite` = `Metabolite`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_reaction_protein`
--

/*!50001 DROP VIEW IF EXISTS `view_reaction_protein`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reaction_protein` AS select distinct `Reaction`.`id` AS `id_reaction`,`Reaction`.`db_identifier` AS `db_identifier_reaction`,`Reaction`.`name` AS `name_reaction`,`Protein`.`db_identifier` AS `db_identifier_protein`,`Protein`.`name` AS `name_protein`,`Protein`.`id` AS `id_protein`,`ReactionInNetwork`.`id_network` AS `id_network` from (((((`Reaction` join `ReactionInNetwork`) join `Catalyses`) join `Enzyme`) join `ProteinInEnzyme`) join `Protein`) where `Reaction`.`id` = `ReactionInNetwork`.`id_reaction` and `Catalyses`.`id_reaction_in_network` = `ReactionInNetwork`.`id` and `Catalyses`.`id_enzyme` = `Enzyme`.`id` and `Enzyme`.`id` = `ProteinInEnzyme`.`id_enzyme` and `Protein`.`id` = `ProteinInEnzyme`.`id_protein` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_stat`
--

/*!50001 DROP VIEW IF EXISTS `view_stat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_bin */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_stat` AS select distinct min(`Pathway`.`id_network`) AS `id_network`,min('pathway') AS `biodata`,count(distinct `Pathway`.`id`) AS `nb` from `Pathway` group by `Pathway`.`id_network` union select min(`ReactionInNetwork`.`id_network`) AS `id_network`,min('reaction') AS `biodata`,count(distinct `ReactionInNetwork`.`id_reaction`) AS `nb` from `ReactionInNetwork` group by `ReactionInNetwork`.`id_network` union select min(`MetaboliteInNetwork`.`id_network`) AS `id_network`,min('metabolite') AS `biodata`,count(distinct `MetaboliteInNetwork`.`id_metabolite`) AS `nb` from `MetaboliteInNetwork` group by `MetaboliteInNetwork`.`id_network` union select min(`Enzyme`.`id_network`) AS `id_network`,min('enzyme') AS `biodata`,count(distinct `Enzyme`.`id`) AS `nb` from `Enzyme` group by `Enzyme`.`id_network` union select min(`Protein`.`id_network`) AS `id_network`,min('protein') AS `biodata`,count(distinct `Protein`.`id`) AS `nb` from `Protein` group by `Protein`.`id_network` union select min(`Gene`.`id_network`) AS `id_network`,min('gene') AS `biodata`,count(distinct `Gene`.`id`) AS `nb` from `Gene` group by `Gene`.`id_network` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-28 14:31:02
